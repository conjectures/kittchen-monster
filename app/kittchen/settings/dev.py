from kittchen.settings.base import *

# All allowed hosts for development settings
ALLOWED_HOSTS = ['*']

# Set debug true for development settings
DEBUG = True
